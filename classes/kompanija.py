from flask_restful import Resource, reqparse
from flask import jsonify
from db import select, insert,update



class Kompanija(Resource):
    def get(self):
        query="select * from Kompanija"
        _, kompanija=select(query)
        return jsonify(kompanija)

    def post(self):
        parser= reqparse.RequestParser()
        parser.add_argument("naziv", required=True,type=str)
        parser.add_argument("sifra", required=True,type=str)
        parser.add_argument("broj_zap", required=True,type=int)
        parser.add_argument("dnevni_limit", required=True,type=float)
        args = parser.parse_args()
        insert(f"""Insert into Kompanija(naziv,sifra,broj_zap,dnevni_limit)
        values('{args['naziv']}','{args['sifra']}',{args['broj_zap']},{args['dnevni_limit']})""")

    def put(self):
        parser= reqparse.RequestParser()
        parser.add_argument("naziv", required=True,type=str)
        parser.add_argument("dnevni_limit", required=True,type=float)
        args = parser.parse_args()
        update(f"""Update Kompanija set dnevni_limit={args['dnevni_limit']}
        where naziv='{args['naziv']}'""")
        