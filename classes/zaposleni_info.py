from ast import arg
from db import select, insert, update
from db import delete_row as delete
from flask_restful import Resource, reqparse

class ZaposleniInfo(Resource):    
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int) # employee id
        args = parser.parse_args()
        query = f"""select z.id, z.kompanija_id, k.dnevni_limit from Zaposleni z, Kompanija k 
                        where z.kompanija_id = k.id and z.id = {args['id']}"""
        _, zaposleni = select(query)
        return zaposleni
