from flask import Flask, jsonify
from flask_restful import Resource,reqparse
import db

class Jelo(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int) # naziv restorana
        args = parser.parse_args()

        _,restoran_data = db.select(f'''select j.id,j.restoran_id,j.naziv,j.opis,j.cijena,j.slika 
        from Jelo j, Restoran r where r.id = j.restoran_id and r.id = {args["id"]}''')
        return jsonify(restoran_data)

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("restoran_id", required = True, type = int)
        parser.add_argument("naziv", required = True, type = str)
        parser.add_argument("opis", required = True, type = str)
        parser.add_argument("cijena", required = True, type = float)
        parser.add_argument("slika", required = True, type = str)
        args = parser.parse_args()
        db.insert(f'''insert into Jelo(restoran_id,naziv,opis,cijena,slika) 
        values({args["restoran_id"]}, "{args["naziv"]}","{args["opis"]}",
        {args["cijena"]},"{args["slika"]}")''')



    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int)
        args = parser.parse_args()
        db.delete_row(f'delete from Jelo where id = {args["id"]}')


    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id",required = True, type = int)
        parser.add_argument("restoran_id", required = True, type = int)
        parser.add_argument("naziv", required = True, type = str)
        parser.add_argument("opis", required = True, type = str)
        parser.add_argument("cijena", required = True, type = float)
        parser.add_argument("slika", required = True, type = str)
        args = parser.parse_args()
        db.update(f'''update Jelo set restoran_id = {args["restoran_id"]},naziv ="{args["naziv"]}",opis ="{args["opis"]}",
        cijena = {args["cijena"]},slika = "{args["slika"]}" where id = {args["id"]}''') 
