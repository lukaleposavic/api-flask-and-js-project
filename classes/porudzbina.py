from flask import Flask,jsonify
from flask_restful import Resource, reqparse
from db import insert,select

class Porudzbina(Resource):
    def post(self):
        #nakon pritiska na dugme Naruci, dodaje se nova porudzbina u tabeli Porudzbine
        #i getuju sve sve porudzbine ostvarene tog dana
        parser=reqparse.RequestParser()
        parser.add_argument('restoran_id',required=True, type=int)
        parser.add_argument('zaposleni_id',required=True, type=int)
        parser.add_argument('jelo_id',required=True, type=int)
        #datum i status ne saljemo jer su defaultno postavljeni na pending i na dan narucivanja
        args = parser.parse_args()
        query=f"""insert into Porudzbine(restoran_id,zaposleni_id,jelo_id)values \
                        ({args['restoran_id']},{args['zaposleni_id']},{args['jelo_id']})"""
        insert(query)
        #zatim je potrebno getovati i vratiti sve porudzbine tog dana
        query=f"""select t1.naziv,t2.naziv,t2.cijena from Porudzbine t,Restoran t1,Jelo t2 where t.restoran_id=t1.id and t.jelo_id=t2.id and t.zaposleni_id={args['zaposleni_id']} and t.datum=curdate()"""
        _, porudzbine = select(query)
        return porudzbine





