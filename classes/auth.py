from tokenize import Double
import pymysql
from flask import Flask, request, jsonify, make_response,render_template
from  werkzeug.security import generate_password_hash, check_password_hash
import jwt
from functools import wraps
# from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import uuid
from flask_restful import Resource, Api, reqparse
from flask import Flask, jsonify
from flask_restful import Resource,reqparse
import db

app = Flask(__name__)
api = Api(app)


# class Zaposleni(Resource):
#     def get(self):
#         _,restoran_data = db.select('select komapnija_id,ime,prezime,username,sifra from Zaposleni')
#         return jsonify(restoran_data)





# decorator for verifying the JWT
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):  
        token = None
        # jwt is passed in the request header
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        # return 401 if token is not passed
        if not token:
            return jsonify({'message' : 'Token is missing !!'}), 401
  
        try:
            # decoding the payload to fetch the stored details
            data = jwt.decode(token, "key")
            current_user = db.select(f"select kompanija_id,ime,prezime,username,sifra from Zaposleni where prezime = '{data['prezime']}' ")
        except:
            return jsonify({
                'message' : 'Token is invalid !!'
            }), 401
        # returns the current logged in users contex to the routes
        return  f(current_user, *args, **kwargs)
  
    return decorated
  
# User Database Route
# this route sends back list of users users
class User(Resource):
    @token_required
    def post(self):
# @app.route('/user', methods =['GET'])
    
    
        # querying the database
        # for all the entries in it
        users = db.select(f"select * from Zaposleni")
        # converting the query objects
        # to list of jsons
        output = []
        for user in users:
            user = user[1][0]
            # appending the user data json
            # to the response list
            output.append({
                # 'id': user.id,
                'ime' : user["ime"],
                'username' : user["username"],
                'prezime' : user["prezime"]
            })
    
        return jsonify({'users': output})



class Get_zaposleni(Resource):
    def get(self):
        query="select * from Zaposleni"
        korisnik = db.select(query)
        return jsonify(korisnik)

  
# route for logging user in
class Login(Resource):
    def post(self):
        # creates dictionary of form data
        try:
            print("aaaaa")
            
            parse = reqparse.RequestParser()
            print(parse)
            parse.add_argument("username", required = True, type = str) 
            parse.add_argument("sifra",required = True, type = str)
            args = parse.parse_args()
            print(args["username"])
            if not args or not args['username'] or not args['sifra']:
                # returns 401 if any email or / and password is missing
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
                )
        
            user = db.select(f"select kompanija_id,ime,prezime,username,sifra from Zaposleni where username = '{args['username']}' and sifra = '{args['sifra']}' ")
        
            if user[0] == 0:
                # returns 401 if user does not exist
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="User does not exist !!"'}
                )
        
            user = user[1][0]

            # generates the JWT Token
            token = jwt.encode({
                'prezime': user["prezime"],
                'exp' : datetime.utcnow() + timedelta(minutes = 30)
            }, "key")

            return make_response(jsonify({'token' : token.decode('UTF-8')}), 201)
        except Exception as e:
            import sys, os
            print('SSSSSSSSSSSSSSSSSSSSss')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )
    # returns 403 if password is wrong
    
  
# signup route
# @app.route('/signup', methods =['POST'])
class Signup(Resource):
    def post(self):
        # creates a dictionary of the form data
        data = reqparse.RequestParser()
    
        # gets name, email and password
        data.add_argument("ime", type = str) 
        data.add_argument("username", required = True, type = str) 
        data.add_argument("sifra", required = True, type = str) 
        data.add_argument("kompanija_id", type = int) 
        data.add_argument("prezime", required = True, type = str) 
        args = data.parse_args()
       

        user = db.select(f"select kompanija_id,ime,prezime,username,sifra from Zaposleni where username = '{args['username']}' ");
        print(user)
        if user[0] == 0:
            # database ORM object
            prezime = str(uuid.uuid4()) 
            # sifra1 = args["sifra"]
            # sifra2 = generate_password_hash(sifra1)
            db.insert(f'''insert into Zaposleni(ime,username,sifra,kompanija_id,prezime)
            values("{args["ime"]}", "{args["username"]}", "{args["sifra"]}", "{args["kompanija_id"]}", "{prezime}")''')
           
            
            return make_response('Successfully registered.', 201)
        else:
            # returns 202 if user already exists
            return make_response('User already exists. Please Log in.', 202)


class Login_restaurant(Resource):
    def post(self):
        # creates dictionary of form data
        try:
         
            parse = reqparse.RequestParser()
            print(parse)
            parse.add_argument("naziv", required = True, type = str) 
            parse.add_argument("sifra",required = True, type = str)
            args = parse.parse_args()
            print(args["naziv"])
            if not args or not args['naziv'] or not args['sifra']:
                # returns 401 if any email or / and password is missing
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
                )
        
            user = db.select(f"select * from Restoran where naziv = '{args['naziv']}' and sifra = '{args['sifra']}' ")
        
            if user[0] == 0:
                # returns 401 if user does not exist
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="User does not exist !!"'}
                )
        
            user = user[1][0]

            # generates the JWT Token
            token = jwt.encode({
                'naziv': user["naziv"],
                'exp' : datetime.utcnow() + timedelta(minutes = 30)
            }, "key")

            return make_response(jsonify({'token' : token.decode('UTF-8')}), 201)
        except Exception as e:
            import sys, os
            print('SSSSSSSSSSSSSSSSSSSSss')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )


class Signup_restaurant(Resource):
    def post(self):
        # creates a dictionary of the form data
        data = reqparse.RequestParser()
    
        # gets name, email and password
        data.add_argument("naziv", type = str) 
        data.add_argument("sifra", required = True, type = str) 
        
        args = data.parse_args()
       

        user = db.select(f"select * from Restoran where naziv = '{args['naziv']}' ");
        print(user)
        if user[0] == 0:
            # database ORM object
            # sifra1 = args["sifra"]
            # sifra2 = generate_password_hash(sifra1)
            db.insert(f'''insert into Restoran(naziv,sifra)
            values("{args["naziv"]}", "{args["sifra"]}" )''')
           
            
            return make_response('Successfully registered.', 201)
        else:
            # returns 202 if user already exists
            return make_response('User already exists. Please Log in.', 202)


class Login_company(Resource):
    def post(self):
        # creates dictionary of form data
        try:
         
            parse = reqparse.RequestParser()
            print(parse)
            parse.add_argument("naziv", required = True, type = str) 
            parse.add_argument("sifra",required = True, type = str)
            args = parse.parse_args()
            print(args["naziv"])
            if not args or not args['naziv'] or not args['sifra']:
                # returns 401 if any email or / and password is missing
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
                )
        
            user = db.select(f"select * from Kompanija where naziv = '{args['naziv']}' and sifra = '{args['sifra']}' ")
        
            if user[0] == 0:
                # returns 401 if user does not exist
                return make_response(
                    'Could not verify',
                    401,
                    {'WWW-Authenticate' : 'Basic realm ="User does not exist !!"'}
                )
        
            user = user[1][0]

            # generates the JWT Token
            token = jwt.encode({
                'naziv': user["naziv"],
                'exp' : datetime.utcnow() + timedelta(minutes = 30)
            }, "key")

            return make_response(jsonify({'token' : token.decode('UTF-8')}), 201)
        except Exception as e:
            import sys, os
            print('SSSSSSSSSSSSSSSSSSSSss')
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno, e)
            return make_response(
                'Bad request',
                400,
                {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
            )


class Signup_company(Resource):
    def post(self):
        # creates a dictionary of the form data
        data = reqparse.RequestParser()
    
        # gets name, email and password
        data.add_argument("naziv", type = str) 
        data.add_argument("sifra", required = True, type = str) 
        data.add_argument("broj_zap", required = True, type = int) 
        data.add_argument("dnevni_limit", type = int) 
        
        args = data.parse_args()
       
        user = db.select(f"select * from Kompanija where naziv = '{args['naziv']}' ");
        
        if user[0] == 0:
            # database ORM object
            # sifra1 = args["sifra"]
            # sifra2 = generate_password_hash(sifra1)
            db.insert(f'''insert into Zaposleni(naziv,sifra,broj_zap,dnevni_limit)
            values("{args["naziv"]}", "{args["sifra"]}", "{args["broj_zap"]}", "{args["dnevni_limit"]}" )''')
           
            
            return make_response('Successfully registered.', 201)
        else:
            # returns 202 if user already exists
            return make_response('User already exists. Please Log in.', 202)


if __name__ == "__main__":
    # setting debug to True enables hot reload
    # and also provides a debugger shell
    # if you hit an error while running the server
    app.run(debug = True)