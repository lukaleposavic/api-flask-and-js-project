from db import select, insert, update
from db import delete_row as delete
from flask_restful import Resource, reqparse

"""
in args_type
  * 0 represents integer type
  * "" empty string represents string
"""

class Zaposleni(Resource):
    def add_arguments(self, args_name, args_type):
        parser = reqparse.RequestParser()
        for i in range(len(args_name)):
            parser.add_argument(f"{args_name[i]}", 
                                required = True, 
                                type = type(args_type[i]))
        return parser.parse_args()
    
    def get(self):
        query = "select * from Zaposleni"
        _, zaposleni = select(query)
        return zaposleni

    def post(self):
        args_name = ["kompanija_id", "ime", "username", "sifra"]
        args_type = [0, "", "", ""]
        args = self.add_arguments(args_name, args_type)
        query = f"""insert into Zaposleni(kompanija_id, ime, username, sifra) 
            values ('{args['kompanija_id']}','{args['ime']}',
            '{args['username']}','{args['sifra']}')"""
        insert(query)

    def put(self):
        args_name = ["id", "kompanija_id" , "ime", "username", "sifra"]
        args_type = [0, 0, "", "", ""]
        args = self.add_arguments(args_name, args_type)
        update(f"""update Zaposleni set kompanija_id = {args['kompanija_id']}, 
                ime = '{args['ime']}', username = '{args['username']}', sifra = '{args['sifra']}'
                where id = {args['id']}""")

    def delete(self):
        args_name = ["id"]
        args_type = [0]
        args = self.proba(args_name, args_type)
        query = f"delete from Zaposleni where id = {args['id']}"
        delete(query)