-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: api_i_flask
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Jelo`
--

DROP TABLE IF EXISTS `Jelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Jelo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restoran_id` int DEFAULT NULL,
  `naziv` varchar(30) DEFAULT NULL,
  `opis` varchar(200) DEFAULT NULL,
  `cijena` double(6,2) DEFAULT NULL,
  `slika` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restoran_id` (`restoran_id`),
  CONSTRAINT `Jelo_ibfk_1` FOREIGN KEY (`restoran_id`) REFERENCES `Restoran` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Jelo`
--

LOCK TABLES `Jelo` WRITE;
/*!40000 ALTER TABLE `Jelo` DISABLE KEYS */;
INSERT INTO `Jelo` VALUES (1,1,'Margarita','Sir,pelat',3.20,'https://i.ibb.co/kQgyXLW/goodfellas1.jpg'),(2,1,'The Boss sendvič','Piletina, sir, pomfrit, ranč sos',4.70,'https://i.ibb.co/Cvv24Y1/goodfellas2.jpg'),(3,1,'Hamburger','100% juneće meso, iceberg salata, paradajz, luk, big boss sos',3.90,'https://i.ibb.co/mDvLRpr/goodfellas3.jpg'),(4,2,'Sicila sendvič','Italijanska ciabatta, grilovana piletina, slanina, sir, puter, zelena salata, paradajz, crne masline, pomfrit',4.40,'https://i.ibb.co/8xZQn7r/sicilia1.jpg'),(5,2,'Corleone slane palacinke','Šunka, sir, sos',3.50,'https://i.ibb.co/SX7fw5p/sicilia2.jpg'),(6,2,'Cezar salata','Grilovana piletina, pančeta, zelena salata, parmezan, jaje, paradajz čeri, dressing',4.90,'https://i.ibb.co/PNHtBh3/sicilia3.jpgk'),(7,3,'Kaprićoza','Pelat, sir, šunka, šampinjoni, origano',3.50,'https://i.ibb.co/41sjSrf/kristal1.jpg'),(8,3,'Ćevapi','10 komada sa prilozima po izboru',4.30,'https://i.ibb.co/JxwSGhh/kristal2.jpg'),(9,3,'Pasta Bolonjeze','Bolonjez ragu, parmezan',4.10,'https://i.ibb.co/tzh1RTL/kristal3.jpg');
/*!40000 ALTER TABLE `Jelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Kompanija`
--

DROP TABLE IF EXISTS `Kompanija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Kompanija` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) DEFAULT NULL,
  `sifra` varchar(200) DEFAULT NULL,
  `broj_zap` int DEFAULT NULL,
  `dnevni_limit` double(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `naziv` (`naziv`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kompanija`
--

LOCK TABLES `Kompanija` WRITE;
/*!40000 ALTER TABLE `Kompanija` DISABLE KEYS */;
INSERT INTO `Kompanija` VALUES (1,'Bixbit','glovo123',20,7.00),(2,'Coinis','donesi123',10,8.00);
/*!40000 ALTER TABLE `Kompanija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Porudzbine`
--

DROP TABLE IF EXISTS `Porudzbine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Porudzbine` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restoran_id` int DEFAULT NULL,
  `zaposleni_id` int DEFAULT NULL,
  `jelo_id` int DEFAULT NULL,
  `datum` date DEFAULT (curdate()),
  `status` varchar(20) DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `restoran_id` (`restoran_id`),
  KEY `zaposleni_id` (`zaposleni_id`),
  KEY `jelo_id` (`jelo_id`),
  CONSTRAINT `Porudzbine_ibfk_1` FOREIGN KEY (`restoran_id`) REFERENCES `Restoran` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Porudzbine_ibfk_2` FOREIGN KEY (`zaposleni_id`) REFERENCES `Zaposleni` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Porudzbine_ibfk_3` FOREIGN KEY (`jelo_id`) REFERENCES `Jelo` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Porudzbine_chk_1` CHECK ((`status` in (_utf8mb4'pending',_utf8mb4'active',_utf8mb4'delivered')))
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Porudzbine`
--

LOCK TABLES `Porudzbine` WRITE;
/*!40000 ALTER TABLE `Porudzbine` DISABLE KEYS */;
INSERT INTO `Porudzbine` VALUES (1,1,1,1,'2022-02-10','delivered'),(2,1,1,2,'2022-02-11','delivered'),(3,1,1,3,'2022-02-14','delivered'),(4,2,1,4,'2022-02-09','delivered'),(5,2,1,5,'2022-02-15','pending'),(6,2,1,6,'2022-02-08','delivered'),(7,3,1,7,'2022-02-07','delivered'),(8,3,1,8,'2022-02-04','delivered'),(9,3,1,9,'2022-02-03','delivered'),(10,2,2,4,'2022-02-09','delivered'),(11,2,2,5,'2022-02-08','delivered'),(12,2,2,6,'2022-02-14','delivered'),(13,1,2,1,'2022-02-10','delivered'),(14,1,2,2,'2022-02-11','delivered'),(15,1,2,3,'2022-02-07','delivered'),(16,3,2,7,'2022-02-15','pending'),(17,3,2,8,'2022-02-04','delivered'),(18,3,2,9,'2022-02-03','delivered'),(19,3,3,7,'2022-02-10','delivered'),(20,3,3,8,'2022-02-11','delivered'),(21,3,3,9,'2022-02-14','delivered'),(22,1,3,1,'2022-02-10','delivered'),(23,1,3,2,'2022-02-07','delivered'),(24,1,3,3,'2022-02-04','delivered'),(25,2,3,4,'2022-02-09','delivered'),(26,2,3,5,'2022-02-15','pending'),(27,2,3,6,'2022-02-08','delivered'),(28,1,4,1,'2022-02-09','delivered'),(29,1,4,2,'2022-02-10','delivered'),(30,1,4,3,'2022-02-14','delivered'),(31,2,4,4,'2022-02-09','delivered'),(32,2,4,5,'2022-02-15','pending'),(33,2,4,6,'2022-02-08','delivered'),(34,3,4,7,'2022-02-07','delivered'),(35,3,4,8,'2022-02-09','delivered'),(36,3,4,9,'2022-02-03','delivered');
/*!40000 ALTER TABLE `Porudzbine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Restoran`
--

DROP TABLE IF EXISTS `Restoran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Restoran` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) DEFAULT NULL,
  `sifra` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `naziv` (`naziv`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Restoran`
--

LOCK TABLES `Restoran` WRITE;
/*!40000 ALTER TABLE `Restoran` DISABLE KEYS */;
INSERT INTO `Restoran` VALUES (1,'Goodfellas Pizzeria & grill','goodfellas123'),(2,'Sicilia','sicilia123'),(3,'Restoran Kristal','kristal123');
/*!40000 ALTER TABLE `Restoran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Zaposleni`
--

DROP TABLE IF EXISTS `Zaposleni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Zaposleni` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kompanija_id` int DEFAULT NULL,
  `ime` varchar(30) DEFAULT NULL,
  `prezime` varchar(50) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `sifra` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `kompanija_id` (`kompanija_id`),
  CONSTRAINT `Zaposleni_ibfk_1` FOREIGN KEY (`kompanija_id`) REFERENCES `Kompanija` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Zaposleni`
--

LOCK TABLES `Zaposleni` WRITE;
/*!40000 ALTER TABLE `Zaposleni` DISABLE KEYS */;
INSERT INTO `Zaposleni` VALUES (1,1,'Marko','Markovic','marko123','marko123'),(2,1,'Petar','Petrovic','petar123','petar123'),(3,2,'Janko','Jankovic','janko123','janko123'),(4,2,'Milos','Milosevic','milos123','milos123');
/*!40000 ALTER TABLE `Zaposleni` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-17 20:13:36