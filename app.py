from flask import Flask, jsonify
from flask_restful import Api
from flask_cors import CORS
from classes.jelo import Jelo
from classes.porudzbina import Porudzbina
from classes.kompanija import Kompanija
from classes.zaposleni import Zaposleni
from classes.restorani import Restoran
from classes.auth import Login,Signup,Login_restaurant,Signup_restaurant,Signup_company,Login_company,User,Get_zaposleni
from classes.zaposleni_info import ZaposleniInfo

app = Flask(__name__)
api = Api(app)

cors = CORS(app)
app.config['CORS-HEADERS'] = 'Content-Type' 

api.add_resource(ZaposleniInfo,'/zaposleni_info')
api.add_resource(Login,'/login')
api.add_resource(Signup,'/signup')
api.add_resource(Login_restaurant,'/loginasres')
api.add_resource(Signup_restaurant,'/signupasres')
api.add_resource(Signup_company,'/signupascompany')
api.add_resource(Login_company,'/signupascompany')
# api.add_resource(Get_zaposleni,'/zaposleni')
api.add_resource(User,'/users')
api.add_resource(Jelo, '/jelo')
api.add_resource(Porudzbina, '/porudzbina')
api.add_resource(Kompanija,'/kompanija')
api.add_resource(Zaposleni, '/zaposleni')
api.add_resource(Restoran,'/restoran')

if __name__ == '__main__':
    app.run(debug = True)
