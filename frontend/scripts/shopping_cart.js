let meals = JSON.parse(localStorage.getItem("shopping_cart"));

let employee_info = JSON.parse(localStorage.getItem("employee_info"));

let dailyBudget = employee_info.dnevni_limit;
let zaposleni_id = employee_info.id;

function showCart() {
  const name = document.getElementById("restaurant-name");
  name.innerHTML = "";
  name.innerHTML = `${meals[0].ime_restorana}`;
  const meal_list = document.getElementById("meal-list");
  meal_list.innerHTML = "";
  var i = 0;
  meals.forEach((item) => {
    let { naziv, quantity, cijena } = item;

    const new_row = document.createElement("li");
    new_row.classList.add("collection-item");
    new_row.classList.add("avatar");

    new_row.innerHTML = `<div class="product-name">${naziv}</div>
        <div class="quantity"><input class="quantity-box" type="number" value="${quantity}" min="1" onchange="updateQuantity(this.value,${i})"style="width:40px;"></div>
        <div class="price">${(quantity * cijena).toFixed(2)} €</div>
        <div class="delete"><button onclick="deleteItem(${i})">X</button></div>`;

    meal_list.appendChild(new_row);
    i += 1;
  });
}

function updateQuantity(quantity, index) {
  meals[index].quantity = quantity;
  showCart();
  calculateTotal();
}

function deleteItem(i) {
  meals.splice(i, 1);
  showCart();
  calculateTotal();
  console.log("Works");
}

function calculateTotal() {
  const sum = document.getElementById("sum");
  sum.classList.add("sum-container");
  sum.innerHTML = "";
  total = 0;
  meals.forEach((item) => {
    let { quantity, cijena } = item;
    total += quantity * cijena;
  });
  sum.innerHTML = `<h5 class="sum">Total ${total.toFixed(2)} €</h5>
    <button id="naruci" onclick="orderFood()">Naruci</button></div>
    <p class="error">Porudzbina je skuplja od vaseg budzeta!</p>`;
  if (total > dailyBudget) {
    let naruci = document.getElementById("naruci");
    naruci.disabled = true;
    let errormsg = document.querySelector(".error");
    errormsg.style.display = "block";
  }
  if (total == 0) {
    let naruci = document.getElementById("naruci");
    naruci.disabled = true;
  }
}

function orderFood() {
  const porudzbine = [];
  meals.forEach((item) => {
    let { restoran_id, id, quantity } = item;
    if (quantity != 1) {
      for (let i = quantity; i > 1; i--) {
        oneRow = {
          restoran_id: restoran_id,
          zaposleni_id: zaposleni_id,
          jelo_id: id,
        };
        porudzbine.push(oneRow);
      }
    }
    oneRow = {
      restoran_id: restoran_id,
      zaposleni_id: zaposleni_id,
      jelo_id: id,
    };
    porudzbine.push(oneRow);
  });

  upisiPorudzbine(porudzbine);
}

async function upisiPorudzbine(porudzbine) {
  let unos = [];
  for (let i = 0; i < porudzbine.length; i++) {
    let { restoran_id, zaposleni_id, jelo_id } = porudzbine[i];
    unos = await axios.post(
      `http://127.0.0.1:5000/porudzbina?restoran_id=${restoran_id}&zaposleni_id=${zaposleni_id}&jelo_id=${jelo_id}`
    );
  }

  napraviFakturu(unos);
}

function upisiNaDokument(doc) {
  // margins are in closure
  let margin_top = 20;
  let margin_left = 20;

  return function (tekst, opt = {}) {
    doc.text(margin_left, margin_top, tekst, opt);
    margin_top += 6;
  };
}

function napraviFakturu(unos) {
  let doc = new jsPDF();
  doc.setFont("helvetica");

  let upis = upisiNaDokument(doc);

  let odvoji = (br_linija = 1) => {
    while (br_linija > 0) {
      upis(" ");
      br_linija--;
    }
  };

  // Set title
  doc.setFontType("bold");
  doc.setFontSize(32);
  upis("Faktura", { align: "center" });
  odvoji(2);

  // Set content
  doc.setFontType("normal");
  doc.setFontSize(15);

  for (let i = 0; i < unos.data.length; i++) {
    restoran_string = "Restoran: " + unos.data[i]["naziv"];
    jelo_string = "Jelo: " + unos.data[i]["t2.naziv"];
    cijena_string = "Cijena: " + unos.data[i]["cijena"].toFixed(2) + " eura";

    upis(jelo_string);
    upis(restoran_string);
    upis(cijena_string);
    odvoji();
  }

  odvoji();
  upis("Prijatno!");

  doc.save("Faktura.pdf");
}

showCart();
calculateTotal();
