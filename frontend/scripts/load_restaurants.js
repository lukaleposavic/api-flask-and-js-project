const restaurants_container = document.getElementById("restaurant-list");

let restaurants = [];

(function () {
  const params = new URLSearchParams(document.location.search);
  const zaposleni_id = parseInt(params.get("id"));

  axios
    .get("http://127.0.0.1:5000/restoran")
    .then((resp) => {
      restaurants = resp.data;
      displayData(zaposleni_id);
    })
    .catch((err) => alert("Doslo je do greske:\n " + err));
})();

async function displayData(zaposleni_id) {
  let employee = await axios.get(
    `http://127.0.0.1:5000/zaposleni_info?id=${zaposleni_id}`
  );
  employee = employee.data[0];

  console.log(employee);
  localStorage.setItem("employee_info", JSON.stringify(employee));

  restaurants_container.innerHTML = "";

  restaurants.forEach((restaurant) => {
    const { id, naziv } = restaurant;

    const restaurant_item = document.createElement("a");
    restaurant_item.classList.add("collection-item");
    restaurant_item.setAttribute("href", `./foods_list.html?id=${id}`);
    restaurant_item.innerHTML = `${naziv}`;

    restaurants_container.appendChild(restaurant_item);
  });
}
