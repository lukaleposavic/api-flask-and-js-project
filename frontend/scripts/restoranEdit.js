const jelo = JSON.parse(localStorage.getItem("food_to_edit"))
ime = document.getElementById("ime")
cijena = document.getElementById("cijena")
opis = document.getElementById("opis")
slika = document.getElementById("slika")

ime.value = jelo.naziv
cijena.value = jelo.cijena
opis.value = jelo.opis
slika.value = jelo.slika

const form = document.getElementById("form")
form.addEventListener("submit",editJelo)

function editJelo(event){
    event.preventDefault();
    console.log(jelo);
    cijena.value = parseFloat(cijena.value)
    fetch(`http://127.0.0.1:5000/jelo?id=${jelo.id}&restoran_id=${jelo.restoran_id}&naziv=${ime.value}&cijena=${cijena.value}&opis=${opis.value}&slika=${slika.value}`,{
    method : 'PUT',
  })
.then((resp) => {

    let izmijenjeno_jelo=[];
    izmijenjeno_jelo.id = jelo.id;
    izmijenjeno_jelo.restoran_id = jelo.restoran_id;
    izmijenjeno_jelo.naziv = ime.value;
    izmijenjeno_jelo.cijena = cijena.value;
    izmijenjeno_jelo.opis = opis.value;
    izmijenjeno_jelo.slika = slika.value;
    localStorage.setItem("izmijenjeno_jelo",JSON.stringify(izmijenjeno_jelo))
    window.location.href='restoran.html?id='+jelo.restoran_id;
})
}