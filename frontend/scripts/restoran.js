let food = [];

(function () {
  const params = new URLSearchParams(document.location.search);
  const restaurant_id = parseInt(params.get("id"));
  fetch("http://127.0.0.1:5000/restoran")
    .then((resp) => resp.json())
    .then((data) => {
      const restaurant = data.filter((resp) => resp.id === restaurant_id)[0];
      getFoodData(restaurant);
    });
})();


function getFoodData(restaurant) {
  document.getElementById("restaurant-name").innerHTML = restaurant.naziv;
  fetch(`http://127.0.0.1:5000/jelo?id=${restaurant.id}`)
    .then((resp) => resp.json())
    .then((data) => {
      food = data;
      displayData();
    });
  }


function displayData() {
  const food_list = document.getElementById("food-list");
  food_list.innerHTML = "";

  food.forEach((item) => {
    let { id, restoran_id, naziv, opis, cijena, slika } = item;

    // Izmijeniti na slika === ""
    if (slika === "") {
      slika = "https://img.freepik.com/free-vector/flying-slice-pizza-cartoon-vector-illustration-fast-food-concept-isolated-vector-flat-cartoon-style_138676-1934.jpg?size=338&ext=jpg&ga=GA1.2.1222885761.1641945600";
    }

    const food_item = document.createElement("li");
    food_item.classList.add("collection-item");
    food_item.classList.add("avatar");

    food_item.innerHTML = `<div class="main-part">
        <img
          src="${slika}"
          alt=""
          class="circle"
        />
        <span class="title">${naziv}</span><br />
        <span id="price" class="teal lighten-1 white-text">
          ${cijena}<i class="tiny material-icons">euro_symbol</i>
        </span>
        <p class="blue-grey-text text-lighten-1">
          ${opis}
        </p>
      </div>
      <div class="buttons-part">
        <a href="#!"onClick="deleteJelo(${id})">
          <i class="material-icons">clear</i>
        </a>
        <a href="#!" onClick ="getJelo(event,${id})">
          <i class="material-icons">create</i>
        </a>
      </div>`;

    food_list.appendChild(food_item);
  });
}


function deleteJelo(id){
  fetch(`http://127.0.0.1:5000/jelo?id=${id}`,{
    method : 'DELETE',
  })
  .then((resp) => {
  
    const removed_item = food.filter(i => id === i.id)[0];
    const indeks = food.indexOf(removed_item)
    food.splice(indeks,1)
    displayData();
 
  });
}





function getJelo(event,id){
  event.preventDefault();
  const food_to_edit = food.filter(i => id=== i.id)[0];
  console.log(food_to_edit)
  localStorage.setItem("food_to_edit",JSON.stringify(food_to_edit));
  window.location.href='restoranEdit.html'
}




