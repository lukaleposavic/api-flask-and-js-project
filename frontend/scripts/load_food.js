let food = [];

(function () {
  const params = new URLSearchParams(document.location.search);
  const restaurant_id = parseInt(params.get("id"));

  axios
    .get("http://127.0.0.1:5000/restoran")
    .then((resp) => {
      const restaurant = resp.data.filter(
        (rest) => rest.id === restaurant_id
      )[0];
      getFoodData(restaurant);
    })
    .catch((err) => alert("Doslo je do greske:\n " + err));
})();

function getFoodData(restaurant) {
  document.getElementById("restaurant-name").innerHTML = restaurant.naziv;

  axios
    .get(`http://127.0.0.1:5000/jelo?id=${restaurant.id}`)
    .then((resp) => {
      food = resp.data.map((obj) => {
        obj["quantity"] = 0;
        obj["ime_restorana"] = restaurant.naziv;
        return obj;
      });
      displayData();
    })
    .catch((err) => alert("Greska, restoran ne postoji:\n " + err));
}

function displayData() {
  const food_list = document.getElementById("food-list");
  food_list.innerHTML = "";

  food.forEach((item) => {
    let { id, restoran_id, naziv, opis, cijena, slika } = item;

    if (slika === "") {
      slika =
        "https://img.freepik.com/free-vector/flying-slice-pizza-cartoon-vector-illustration-fast-food-concept-isolated-vector-flat-cartoon-style_138676-1934.jpg?size=338&ext=jpg&ga=GA1.2.1222885761.1641945600";
    }

    const food_item = document.createElement("li");
    food_item.classList.add("collection-item");
    food_item.classList.add("avatar");

    food_item.innerHTML = `<div class="main-part">
        <img
          src="${slika}"
          alt=""
          class="circle"
        />
        <span class="title">${naziv}</span><br />
        <span id="price" class="teal lighten-1 white-text">
          ${cijena.toFixed(2)}<i class="tiny material-icons">euro_symbol</i>
        </span>
        <p class="blue-grey-text text-lighten-1">
          ${opis}
        </p>
      </div>
      <div class="buttons-part">
        <a href="#!" onclick="putItem(event, ${id}, -1)">
          <i class="material-icons">remove_circle_outline</i>
        </a>
        <span id="quantity-${id}">0</span>
        <a href="#!" onclick="putItem(event, ${id})">
          <i class="material-icons">add_circle_outline</i>
        </a>
      </div>`;

    food_list.appendChild(food_item);
  });
}

function putItem(event, food_id, action = 1) {
  event.preventDefault();

  const food_index = food.findIndex((item) => item.id === food_id);

  let quantity = food[food_index].quantity;
  action === 1 ? quantity++ : quantity--;

  if (quantity < 0) {
    quantity = 0;
  }

  food[food_index].quantity = quantity;
  document.getElementById("quantity-" + food_id).innerHTML =
    food[food_index].quantity;
}

function saveToCart() {
  // Filter array food, if quantity is 0 don't add to cart storage
  const cart_food = food.filter((item) => item.quantity > 0);
  localStorage.setItem("shopping_cart", JSON.stringify(cart_food));

  window.location.href = "./shopping_cart.html";
}
